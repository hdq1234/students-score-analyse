export const subjectMap = [
  {
    name: '语文',
    key: 'chinese',
    type: 1,
    fullScore: 150,
    scoreKey: '语文',
    rankKey: '语名'
  },
  {
    name: '数学',
    key: 'mathematics',
    type: 2,
    fullScore: 150,
    scoreKey: '数学',
    rankKey: '数名'
  },
  {
    name: '英语',
    key: 'english',
    type: 3,
    fullScore: 150,
    scoreKey: '英语',
    rankKey: '英名'
  },
  {
    name: '物理',
    key: 'physical',
    type: 4,
    fullScore: 100,
    scoreKey: '物理',
    rankKey: '物名'
  },
  {
    name: '化学',
    key: 'chemical',
    type: 5,
    fullScore: 100,
    scoreKey: '化学',
    rankKey: '化名'
  },
  {
    name: '政治',
    key: 'political',
    type: 6,
    fullScore: 100,
    scoreKey: '政治',
    rankKey: '政名'
  },
  {
    name: '历史',
    key: 'history',
    type: 7,
    fullScore: 100,
    scoreKey: '历史',
    rankKey: '历名'
  },
  {
    name: '生物',
    key: 'biological',
    type: 8,
    fullScore: 100,
    scoreKey: '生物',
    rankKey: '生名'
  },
  {
    name: '地理',
    key: 'geographic',
    type: 9,
    fullScore: 100,
    scoreKey: '地理',
    rankKey: '地名'
  }
]
export const classMap = [
  {
    label: '1班',
    value: '1班',
    bgColor: '#d32525'
  },
  {
    label: '2班',
    value: '2班',
    bgColor: '#0085ff'
  },
  {
    label: '3班',
    value: '3班',
    bgColor: '#9361dd'
  },
  {
    label: '4班',
    value: '4班',
    bgColor: '#d8810b'
  },
  {
    label: '5班',
    value: '5班',
    bgColor: '#5ed80b'
  },
  {
    label: '6班',
    value: '6班',
    bgColor: '#cf3576'
  },
  {
    label: '7班',
    value: '7班',
    bgColor: '#49d4a5'
  },
  {
    label: '8班',
    value: '8班',
    bgColor: '#957e62'
  },
  {
    label: '9班',
    value: '9班',
    bgColor: '#3939fa'
  },
  {
    label: '10班',
    value: '10班',
    bgColor: '#cc66ad'
  },
  {
    label: '11班',
    value: '11班',
    bgColor: '#78a8dd'
  },
  {
    label: '12班',
    value: '12班',
    bgColor: '#b1ba13'
  },
  {
    label: '13班',
    value: '13班',
    bgColor: '#ff00eb'
  },
  {
    label: '14班',
    value: '14班',
    bgColor: '#9fb7cc'
  },
  {
    label: '15班',
    value: '15班',
    bgColor: '#9fb7cc'
  },
  {
    label: '16班',
    value: '16班',
    bgColor: '#9fb7cc'
  },
  {
    label: '17班',
    value: '17班',
    bgColor: '#9fb7cc'
  },
  {
    label: '年段',
    value: '年段',
    bgColor: '#8b0000'
  }
]

export const totalMap = [
  {
    name: '总分',
    key: 'total',
    fullScore: 0,
    scoreKey: '总分',
    rankKey: '段名',
    isTotal: true
  },
  {
    name: '折总',
    key: 'convert',
    fullScore: 0,
    scoreKey: '折总',
    rankKey: '折算名',
    isTotal: true
  }
]
